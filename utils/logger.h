#ifndef JARGON_LOGGER_H
#define JARGON_LOGGER_H

#include "list.h"

enum logger_level {
	WARNING,
	MESSAGE,
	ERROR
};

typedef struct {
	linked_list logs;
	char *log_dump_path;
	int level;
} logger_t;

static logger_t debug_logger;
static logger_t error_logger;
static logger_t warn_logger;

/// @param level the level of the logger (WARNING, MESSAGE or ERROR)
/// @param path the path where to dump the logs
/// @brief returns the logger instance corresponding to the logger level
logger_t *get_logger(const int level, const char* path);

/// @param logger the logger to use
/// @param message the message string to log
/// @brief logs the given message through the specified logger
void log_message(logger_t *logger, const char *message);

/// @param message the message to print
/// @brief logs the given message to the error logger
void log_error(const char *message);

/// @param message the message to print
/// @brief logs the given message to the debug logger
void log_debug(const char *message);

/// @param message the message to print
/// @brief logs the given message to the warning logger
void log_warning(const char *message);

/// @param logger the logger to use
/// @brief Dumps all logs of the same level as the logger into the path
/// specified in the provided logger
void dump_log(logger_t *logger);

/// @brief clears all log entries stored in memory
void clear_logs();

#endif
