#ifndef JARGON_DEF_H
#define JARGON_DEF_H

#include <stdbool.h>
#include <stdint.h>

#define nullptr 0

#define RETURN_FAILURE -1
#define RETURN_SUCCESS 0

typedef uint8_t byte;

#endif
