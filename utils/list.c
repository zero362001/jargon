#include "list.h"
#include <stdlib.h>

void push_back(linked_list *list, void *item) {
	list_node *node = malloc(sizeof(list_node));
	node->next = nullptr;
	node->prev = list->tail;
	node->data = item;
	if (list->tail == nullptr) {
		list->tail = node;
	} else {
		list->tail->next = node;
		list->tail = node;
	} 
	if (list->head == nullptr) list->head = node;
	list->size++;
}

void push_front(linked_list *list, void *item) {
	list_node *node = malloc(sizeof(list_node));
	node->next = list->head;
	node->prev = nullptr;
	node->data = item;
	if (list->head == nullptr) {
		list->head = node;
	} else {
		list->head->prev = node;
		list->head = node;
	} 
	if (list->tail == nullptr) list->tail = node;
	list->size++;
}

void *pop_back(linked_list *list) {
	if (len(list) == 0) return nullptr;
	else {
		list_node *last = list->tail;
		if (list->head == list->tail) list->head = nullptr;
		list->tail = list->tail->prev;
		if (list->tail != nullptr) list->tail->next = nullptr;
		list->size--;
		return last->data;
	}
}

void *pop_front(linked_list *list) {
	if (len(list) == 0) return nullptr;
	else {
		list_node *first = list->head;
		if (list->head == list->tail) list->tail = nullptr;
		list->head = list->head->next;
		if (list->head != nullptr) list->head->prev = nullptr;
		list->size--;
		return first->data;
	}
}

void clear(linked_list *list) {
	list_node *iter = list->head;
	while (iter != nullptr) {
		list_node *next = iter->next;
		if (iter->next != nullptr) 
			((list_node*)iter->next)->prev = nullptr;
		free(iter);
		iter = next;
	}
	list->head = nullptr;
	list->tail = nullptr;
}

list_node *get_head(linked_list *list) {
	return list->head;
}

list_node *get_tail(linked_list *list) {
	return list->tail;
}

void *get_front(linked_list *list) {
	return list->head->data;
}

void *get_back(linked_list *list) {
	return list->tail->data;
}

uint32_t len(linked_list* list) {
	return list->size;
}

bool is_empty(linked_list *list) {
	return list->size == 0;
}
