#ifndef JARGON_LIST_H
#define JARGON_LIST_H

#include "def.h"
#include <stdlib.h>

typedef struct list_node_struct {
	void *next;
	void *prev;
	void *data;
} list_node;

typedef struct list {
	list_node *head;
	list_node *tail;
	uint32_t size;
} linked_list;


/// @param data Pointer to the data that needs to be inserted
/// @brief Inserts the data pointed by 'data' at the back of the list
void push_back(linked_list *list, void *data);

/// @param data Pointer to the data that needs to be inserted
/// @brief Inserts the data pointed by 'data' at the front of the list
void push_front(linked_list *list, void *data);

/// @brief Deletes the data at the back of the list and returns it
void *pop_back(linked_list *list);

/// @brief Deletes the data at the front of the list and returns it
void *pop_front(linked_list *list);

/// @brief Deletes all data from 'list'
void clear(linked_list *list);

/// @brief Returns a node pointer to the first node in the list
list_node *get_head(linked_list *list);

/// @brief Returns a node pointer to the last node in the list
list_node *get_tail(linked_list *list);

/// @brief Returns a void pointer to the data in the first node in the list
void *get_front(linked_list *list);

/// @brief Returns a void pointer to the data in the last node in the list
void *get_back(linked_list *list);

/// @brief Returns the number of items in the list
uint32_t len(linked_list *list);

/// @brief Returns true if there are no items in the list
bool is_empty(linked_list *list);

#endif