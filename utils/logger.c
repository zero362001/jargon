#include "logger.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

static logger_t debug_logger;
static logger_t error_logger;
static logger_t warn_logger;

const char *COL_ERR = "\033[;31m";
const char *COL_WARN = "\033[;33m";
const char *COL_RESET = "\033[0m";

logger_t *get_logger(const int level, const char *path) {
	if (level == MESSAGE) {
		if (debug_logger.log_dump_path == nullptr)
			free(debug_logger.log_dump_path);
		debug_logger.log_dump_path = malloc(sizeof(char) * (strlen(path) + 20));
		debug_logger.level = level;
		strcpy(debug_logger.log_dump_path, path);
		return &debug_logger;
	} else if (level == WARNING) {
		if (warn_logger.log_dump_path == nullptr)
			free(warn_logger.log_dump_path);
		warn_logger.log_dump_path = malloc(sizeof(char) * (strlen(path) + 20));
		warn_logger.level = level;
		strcpy(warn_logger.log_dump_path, path);
		return &warn_logger;
	} else {
		if (error_logger.log_dump_path == nullptr)
			free(error_logger.log_dump_path);
		error_logger.log_dump_path = malloc(sizeof(char) * (strlen(path) + 20));
		error_logger.level = level;
		strcpy(error_logger.log_dump_path, path);
		return &error_logger;
	}
}

void dump_log(logger_t* logger) {
	if (logger->level == ERROR || 
			logger->level == WARNING) {
		if (len(&logger->logs) > 0) {
			FILE *file;
			time_t t = time(NULL);
			struct tm tm = *localtime(&t);
			const char *log_level_str = (
					logger->level == ERROR ? "error" : "warn"
			);
			char *filename = malloc(sizeof(char) * (
					strlen(logger->log_dump_path) + 20));
			sprintf(filename, "%s/%d:%d:%d_%d-%d-%d_log_%s.txt", 
					logger->log_dump_path, tm.tm_hour, tm.tm_min, tm.tm_sec,
					tm.tm_mday, tm.tm_mon, tm.tm_year + 1990, log_level_str);
			file = fopen(filename, "w");
			if (file != nullptr) {
				list_node *iter = get_head(&logger->logs);
				while (iter != nullptr) {
					fprintf(file, "%s", (const char*)iter->data);
					iter = iter->next;
				}
				clear(&logger->logs);
			}
			free(filename);
		}
	}
}

void log_message(logger_t *logger, const char *message) {
	const char *level = logger->level == MESSAGE ? 
			"Debug" : (logger->level == ERROR ?
			"Error" : "Warning");
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	char *message_timestamped = malloc(sizeof(char) * (strlen(message) + 50));
	sprintf(message_timestamped, "%d:%d:%d (%s) := %s\n", tm.tm_hour, tm.tm_min,
			tm.tm_sec, level, message);
	push_back(&logger->logs, message_timestamped);
	if (logger->level == ERROR) {
		fprintf(stderr, "%s%s%s", COL_ERR, message_timestamped, COL_RESET);
	}
	else {
		printf("%s%s%s", logger->level == WARNING ? 
				COL_WARN : COL_RESET, message_timestamped, COL_RESET);
	}
	free(message_timestamped);
}

void log_warning(const char *message) {
	log_message(&warn_logger, message);
}

void log_error(const char *message) {
	log_message(&error_logger, message);
}

void log_debug(const char *message) {
	log_message(&debug_logger, message);
}

void clear_logs() {
	clear(&debug_logger.logs);
	clear(&error_logger.logs);
	clear(&warn_logger.logs);
}