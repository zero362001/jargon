set(TARGET_NAME jargon-lang)
set(ROOT_DIR ${CMAKE_PROJECT_ROOT})
set(SOURCE_FILES src/tree.c src/parse_tree.c 
		src/types.c src/parser.c src/file_parser.c src/interpreter.c
		src/stack.c)
set(HEADER_FILES include/stack.h include/tree.h 
		include/parse_tree.h include/parser.h include/file_parser.h)
add_library(${TARGET_NAME} ${HEADER_FILES} ${SOURCE_FILES})
target_include_directories(${TARGET_NAME} PUBLIC ../utils)
target_include_directories(${TARGET_NAME} PUBLIC include)
target_link_libraries(${TARGET_NAME} jargon-vm)
source_group(
	TREE "${PROJECT_SOURCE_DIR}/lang/include"
	PREFIX "Header files"
	FILES ${HEADER_FILES})
