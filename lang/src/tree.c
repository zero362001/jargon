#include "tree.h"
#include "list.h"

tree_node_binary_t *add_node_left(tree_t *tree, tree_node_binary_t *parent_node, void *data) {
    if (tree != nullptr) {
            if (parent_node->left_child) {
            return parent_node->left_child;
        } else {
            tree_node_binary_t *new_node = malloc(sizeof(tree_node_binary_t));
            if (get_root(tree) == nullptr) parent_node = get_root(tree);
            new_node->data = data;
            new_node->parent_node = parent_node;
            parent_node->left_child = new_node;
            tree->newest = new_node;
            return new_node;
        }
    } else return nullptr;
}

tree_node_binary_t *add_node_right(tree_t *tree, tree_node_binary_t *parent_node, void *data) {
    if (tree != nullptr) {
        if (parent_node->right_child) {
            return parent_node->right_child;
        } else {
            tree_node_binary_t *new_node = malloc(sizeof(tree_node_binary_t));
            if (get_root(tree) == nullptr) parent_node = get_root(tree);
            new_node->data = data;
            new_node->parent_node = parent_node;
            parent_node->right_child = new_node;
            tree->newest = new_node;
            return new_node;
        }
    } else return nullptr;
}

tree_node_binary_t *get_newest(const tree_t *tree) {
    return tree->newest;
}

tree_node_binary_t *get_root(const tree_t *tree) {
    return tree->root_node;
}

void *get_data_root(const tree_t *tree) {
    return tree->root_node->data;
}

void clear_subtree(tree_t *tree, tree_node_binary_t *subtree) {
    if (subtree == nullptr) return;
    if (tree->root_node == subtree) tree->root_node = nullptr;
    if (tree->newest == subtree) tree->newest = nullptr;
    if (subtree->parent_node != nullptr) {
        if (subtree->parent_node->left_child == subtree)
            subtree->parent_node->left_child = nullptr;
        else subtree->parent_node->right_child = nullptr;
    }
    linked_list *to_free = malloc(sizeof(linked_list));
	push_back(to_free, subtree);
	list_node *iter = get_front(to_free);
	while (get_front(to_free) != nullptr) {
        tree_node_binary_t *node = (tree_node_binary_t*) iter->data;
		push_back(to_free, node->left_child);
		push_back(to_free, node->right_child);
        free(node);
        free(node->data);
		free(iter);
		iter = get_front(to_free);
	}
	clear(to_free);
}
