#include "def.h"
#include "logger.h"
#include "parser.h"
#include "stack.h"
#include "parse_tree.h"
#include <string.h>
#include <stdio.h>


const char *token_type_string[17] = {
	"comment",
	"string_literal",
	"char_literal",
	"integer_literal",
	"real_number_literal",
	"curly_brace_open",
	"curly_brace_close",
	"parenthesis_open",
	"parenthesis_close",
	"square_brace_open",
	"square_brace_close",
	"whitespace",
	"operator",
	"keyword",
	"identifier",
	"data_type",
	"semicolon"
};

int clean() {
	clear_logs();
	return 0;
}

int main(void) {
	logger_t *debug_logger = get_logger(MESSAGE, "./");
	logger_t *error_logger = get_logger(ERROR, "./");
	log_message(debug_logger, "Started interpreter...");
	printf("Start typing code...\n");
	char line[200];
	while (true) {
		printf(">> ");
		fgets(line, 200, stdin);
		if (strcmp(line, "quit\n") == 0) break;
		else {
			stack_t *variable_stack = create_stack(127);
			tree_t *parse_tree = (tree_t*) malloc(sizeof(tree_t));
			code_line_t code_line = { "", 0, strlen(line) };
			strcpy(code_line.text, line);
			printf("Lexed line:\n");
			while (!scan_reached_end(&code_line)) {
				lexical_construct_t lexed = lex(&code_line);
				if (lexed.parse_error) {
					log_message(error_logger, "Failed to parse line!");
					break;
				}
				if (lexed.token_type != TOKEN_WHITESPACE) {
					printf("\t%s: \'%s\'\n", 
							token_type_string[lexed.token_type], lexed.word);
					switch(lexed.token_type) {
						TOKEN_INTEGER_LITERAL: {
							if (parse_tree->newest != nullptr) {
								expression_t *exp = parse_tree->newest->data;
								if (exp == EXPRESSION_OPERATION_ARITHMETIC_LOGIC_BINARY) {
									// TODO: Move all this code to the parser.c file, seriously

									// expression_t *int_node = (expression_t*) malloc(sizeof(expression_t));
									// int_node->expression_type = EXPRESSION_DATA_LITERAL;
									// add_node_right(parse_tree, parse_tree->newest, int_node);
									// Push to stack first
								}
							}
							break;
						}
					}
				}
			}
		}
	}
	return clean();
}
