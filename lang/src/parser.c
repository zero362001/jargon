#include "parser.h"
#include "parse_tree.h"
#include "logger.h"
#include "string.h"
#include "ctype.h"
#include "types.h"

#pragma region constants
const char *OPERATORS = "+-/*=:";
const char *KEYWORDS[20] = { 
	"function", 
	"break", 
	"return", 
	"import",
	"pass",
	"operator",
	"struct",
	"match",
	"case",
	"if",
	"elif",
	"else",
	"for",
	"while"
};
const char *DATA_TYPES[5] = {
	"int",
	"real",
	"char",
	"string",
	"bool"
};
const char *BRACES = "[](){}";
const char *TERMINALS = ";\t\n [](){}+-/*=:";

const byte LOGICAL_OR = 0;
const byte LOGICAL_AND = 1;
const byte OR = 2;
const byte AND = 3;
const byte XOR = 4;
const byte EQUALS = 5;
const byte GREATER_THAN = 6;
const byte LESS_THAN = 6;
const byte ATLEAST = 6;
const byte ATMOST = 6;
const byte SHIFT_RIGHT = 7;
const byte SHIFT_LEFT = 7;
const byte ADDITION = 8;
const byte SUBTRACTION = 8;
const byte MULTIPLICATION = 9;
const byte DIVISION = 9;
const byte LOGICAL_NEGATE = 10;
const byte ARITHMETIC_NEGATE = 10;
const byte TYPE_SPEC = 11;
const byte SCOPE_RESOLUTION = 12;
#pragma endregion constants

enum statement_type_enum {
	ASSIGNMENT,
	FUNCTION_CALL,
	FUNCTION_DEFINITION,
	CONTROL_LOOP,
	CONTROL_BRANCH,
	CONTROL_JUMP,
	ASSERTION,
	QUIT
};

bool check_precedence(const byte left, const byte right) {
	return right > left;
}

tree_t *parse(code_line_t *line) {
	tree_t *parse_tree = malloc(sizeof(tree_t));
	while (!scan_reached_end(line)) {
		lexical_construct_t lexed = lex(line);
		if (lexed.parse_error) {
			log_message(get_logger(ERROR, "./"), "Failed to parse expression!");
			free(parse_tree);
			return nullptr;
		} else {
			if (get_root(parse_tree) == nullptr) {
				lexical_construct_t *data = malloc(sizeof(lexical_construct_t));
				data->parse_error = false;
				data->token_type = lexed.token_type;
				strcpy(data->word, lexed.word);
				add_node_left(parse_tree, get_newest(parse_tree), data);
			}
		}
	}
	return parse_tree;
}

bool contains(const char *string, const char c) {
	int x = -1;
	while (string[++x] != '\0') {
		if (string[x] == c) return true;
	}
	return false;
}

bool string_in(const char **list, const char *string, const int count) {
	uint16_t len = (int16_t) (sizeof(list) / sizeof(list[0]));
	int x = -1;
	while (++x < count) {
		if (strcmp(list[x], string) == 0) return true;
	}
	return false;
}

bool is_valid_identifier(const char *string) {
	int x = -1;
	while (string[++x] != '\0') {
		if (x == 0) {
			if (!(isalpha(string[x]) || string[x] == '_')) {
				return false;
			}
		} else {
			if (!(isalnum(string[x]) || string[x] == '_')) {
				return false;
			}
		}
	}
	return true;
}

bool is_integer(const char *string) {
	int x = -1;
	while (string[++x] != '\0') {
		if (string[x] > '9' || string[x] < '0') return false;
	}
	return true;
}

bool is_real_number(const char *string) {
	int x = -1;
	int16_t dec = -1;
	while (string[++x] != '\0') {
		if (string[x] > '9' || string[x] < '0') {
			if (string[x] == '.') {
				if (dec >= 0) return false;
				else dec = x;
			}
		}
	}
}

char scan_next(code_line_t *line) {
	if (line->cursor >= line->length) return '\0';
	else return line->text[line->cursor++];
}

char scan_prev(code_line_t *line) {
	if (line->cursor < 0 || line->cursor > line->length) 
		return '\0';
	else 
		return line->text[line->cursor--];
}

char scan_seek(code_line_t *line, uint16_t pos) {
	if (pos > line->length) {
		line->cursor = line->length;
		return line->text[line->length - 1];
	} else {
		line->cursor = pos;
		return line->text[pos];
	}
}

bool scan_reached_end(const code_line_t *line) {
	return line->cursor == line->length;
}

bool string_contains(const char *array, const char c) {
	for (int x = 0; x < strlen(array); x++) {
		if (array[x] == c) return true;
	}
	return false;
}

lexical_construct_t parse_expression(code_line_t *line, 
		tree_node_binary_t *parent_node) {
	tree_node_binary_t *term_subtree;
	lexical_construct_t term = parse_term(line, term_subtree);
	if (!term.parse_error) {
		char scanned = scan_next(line);
		if (contains(OPERATORS, scanned)) {
			char scanned_ahead = scan_next(line);
			if (contains("&|:<>=", scanned)) {
				if (scanned == scan_next(line)) {
					switch (scanned) {
						case '&': {
							expression_t* exp = (expression_t*) parent_node->data;
							if (check_precedence(exp->operation_code, LOGICAL_AND)) {

							}
							break;
						}
						case '|': {
							break;
						}
						case ':': {
							break;
						}
						case '<': {
							break;
						}
						case '>': {
							break;
						}
						default:
							break;
					}
				}
				else {
					if (scanned == '<' && scanned_ahead == '=') {

					} else if (scanned == '>' && scanned_ahead == '=') {

					} else {
						scan_prev(line);
						switch (scanned) {
							case '&': {
								break;
							}
							case '|': {
								break;
							}
							case ':': {
								break;
							}
							case '<': {
								break;
							}
							case '>': {
								break;
							}
							case '+': {
								break;
							}
							case '-': {
								break;
							}
							case '*': {
								break;
							}
							case '/': {
								break;
							}
							case '^': {
								break;
							}
							default:
								break;
						}
					}
				}
			}
		}
	}
}

lexical_construct_t parse_term(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_literal(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_integer(code_line_t *line, 
		tree_node_binary_t *parent_node) {
	lexical_construct_t lexed;
	tree_node_binary_t *node;
	char scanned = scan_next(line);
	unsigned parsed = 0;
	if (scanned > '9' || scanned < '0') {
		lexed.parse_error = true;
		return lexed;
	}
	while (scanned >= '0' && scanned <= '9' && !scan_reached_end(line)) {
		parsed = (parsed * 10) + (scanned - '0');
		scanned = scan_next(line);
	}
	if (!scan_reached_end(line)) scan_prev(line);
	int *data = malloc(sizeof(int));
	*data = parsed;
	node = literal_node(data, TYPE_INT);
	lexed.parse_error = false;
	lexed.token_type = TOKEN_INTEGER_LITERAL;
	return lexed;
}

lexical_construct_t parse_real(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_char(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_string(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_bool(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_arguments(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_control(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_definition(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_parameter_list(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_id(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_assignment(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t parse_list(code_line_t *line, 
		tree_node_binary_t *parent_node) {}

lexical_construct_t lex(code_line_t *line) {
	char word[100] = { '\0' };
	uint8_t char_iter = 0;
	char c = scan_next(line);
	while (c != '\0') {
		if (c == '"') {
			if (char_iter) {
				logger_t *error_logger = get_logger(ERROR, "./");
				log_message(error_logger, "Malformed statement! \" is reserved for bounding strings with.");
				lexical_construct_t lexed;
				lexed.parse_error = true;
				return lexed;
			}
			c = scan_next(line);
			while (c != '\0') {
				if (c == '"') {
					word[char_iter] = '\0';
					lexical_construct_t lexed = {
						"", TOKEN_STRING_LITERAL, false 
					};
					strcpy(lexed.word, word);
					return lexed;
				} else {
					word[char_iter++] = c;
				}
			}
		} else if (c == '#') {
			if (char_iter) {
				logger_t *error_logger = get_logger(ERROR, "./");
				log_message(error_logger, "Malformed statement! Comments must be started after terminating a statement");
				lexical_construct_t lexed;
				lexed.parse_error = true;
				return lexed;
			}
			scan_seek(line, 200); // Get to the end of the line
			lexical_construct_t lexed = {
				"", TOKEN_COMMENT, false
			};
			return lexed;
		} else if (string_contains(TERMINALS, c)) {
			if (strlen(word) == 0) {
				if (c == '\t' || c == ' ' || c == '\n') {
					lexical_construct_t lexed = {
						"", TOKEN_WHITESPACE, false
					};
					return lexed;
				} else if (string_contains(OPERATORS, c)) {
					if (c == ':') {
						char c_next = scan_next(line);
						if (c_next == ':') {
							// return scope operator
						} else scan_prev(line);
					}
					lexical_construct_t lexed = {
						{ 'o', '\0' }, TOKEN_OPERATOR, false
					};
					lexed.word[0] = c;
					return lexed;
				} else if (string_contains(BRACES, c)) {
					// match operator
					byte brace_type;
					lexical_construct_t lexed = {
						"", brace_type, false
					};
					return lexed;
				} else if (c == ';') {
					lexical_construct_t lexed = {
						"", TOKEN_END_INSTRUCTION, false
					};
					return lexed;
				} else {
					lexical_construct_t lexed;
					lexed.parse_error = true;
					logger_t *error_logger = get_logger(ERROR, "./");
					log_message(error_logger, "Unexpected statement!");
					return lexed;
				}
			}
			else {
				scan_prev(line);
				lexical_construct_t lexed;
				word[char_iter] = '\0';
				strcpy(lexed.word, word);
				lexed.parse_error = false;
				if (string_in(KEYWORDS, word, 7))
					lexed.token_type = TOKEN_KEYWORD;
				else if (string_in(DATA_TYPES, word, 5))
					lexed.token_type = TOKEN_DATA_TYPE;
				else if (is_integer(word))
					lexed.token_type = TOKEN_INTEGER_LITERAL;
				else if (is_real_number(word))
					lexed.token_type = TOKEN_REAL_NUMBER_LITERAL;
				else if (is_valid_identifier(word))
					lexed.token_type = TOKEN_IDENTIFIER;
				else {
					lexed.parse_error = true;
					logger_t *error_logger = get_logger(ERROR, "./");
					log_message(error_logger, "Not a valid keyword, literal or identifier!");
				}
				return lexed;
			}
		} else {
			word[char_iter++] = c;
		}
		c = scan_next(line);
	}
}