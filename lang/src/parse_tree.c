#include "parse_tree.h"
#include "stdlib.h"

tree_node_binary_t *operation_node_binary(tree_node_binary_t *left,
		tree_node_binary_t *right) {
	tree_node_binary_t *new_node = (tree_node_binary_t*) 
			malloc(sizeof(tree_node_binary_t));
	new_node->data = malloc(sizeof(expression_t));
	((expression_t*) new_node->data)->expression_type = 
			EXPRESSION_OPERATION_ARITHMETIC_LOGIC_BINARY;
	new_node->left_child = left;
	new_node->right_child = right;
	return new_node;
}

tree_node_binary_t *operation_node_unary(tree_node_binary_t *node) {
	tree_node_binary_t *new_node = (tree_node_binary_t*) 
			malloc(sizeof(tree_node_binary_t));
	new_node->data = malloc(sizeof(expression_t));
	((expression_t*) node->data)->expression_type = 
			EXPRESSION_OPERATION_ARITHMETIC_LOGIC_UNARY;
	new_node->left_child = node;
	return new_node;
}

tree_node_binary_t *literal_node(void *data, byte type) {
	tree_node_binary_t *node = (tree_node_binary_t*)
			malloc(sizeof(tree_node_binary_t));
	expression_t *exp = malloc(sizeof(expression_t));
	exp->data = data;
	exp->data_type = type;
	exp->expression_type = EXPRESSION_DATA_LITERAL;
	exp->operation_code = 99;
	exp->evaluate = evaluate_literal;
	node->data = exp;
}

tree_node_binary_t *definition_node(tree_node_binary_t *signature,
		tree_node_binary_t *param_list) {
	tree_node_binary_t *new_node = (tree_node_binary_t*)
			malloc(sizeof(tree_node_binary_t));
	new_node->data = malloc(sizeof(expression_t));
	((expression_t*) new_node->data)->expression_type = 
			EXPRESSION_DEFINITION;
}