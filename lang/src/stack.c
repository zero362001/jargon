#include "stack.h"
#include "list.h"
#include "logger.h"

stack_t *create_stack(const uint16_t size) {
	stack_t *new_stack = malloc(sizeof(stack_t));
	new_stack->stack_max_size = size;
	new_stack->stack_count = 0;
	new_stack->top = nullptr;
	new_stack->stack_items = (linked_list*) malloc(sizeof(linked_list));
	return new_stack;
}

void clear_stack(stack_t *stack) {
	clear(stack->stack_items);
	stack->stack_items = nullptr;
	stack->stack_max_size = 0;
	stack->top = nullptr;
}

bool push(stack_t *stack, void *data) {
	if (stack->stack_count >= stack->stack_max_size) {
		logger_t *error_logger = get_logger(ERROR, "./");
		log_message(error_logger, "Stack overflow!");
		return false;
	}
	push_back(stack->stack_items, data);
	stack->stack_count++;
	stack->top = get_tail(stack->stack_items);
}

void* pop(stack_t *stack) {
	if (stack->stack_count <= 0) {
		logger_t *error_logger = get_logger(ERROR, "./");
		log_message(error_logger, "Stack underflow!");
		return nullptr;
	}
	void *data =  pop_back(stack->stack_items);
	stack->stack_count--;
	stack->top = get_tail(stack->stack_items);
}

void *get_top(const stack_t *stack) {
	return get_tail(stack);
}