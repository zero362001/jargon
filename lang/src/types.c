#include "types.h"
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

double parse_real(const byte *bytes, const byte size) {
	// TODO: Implement method
	return 0;
}

void parse_variable(variable_t *var, ...) {
	assert(var != nullptr); // TODO: change to custom assertion later
	va_list arglist;
	va_start(arglist, var);
	switch(var->data_type) {
		case TYPE_BOOLEAN: {
			bool *x = va_arg(arglist, bool*);
			memcpy(x, var->data, sizeof(byte));
			break;
		}
		case TYPE_CHARACTER: {
			char *c = va_arg(arglist, char*);
			memcpy(c, var->data, sizeof(byte));
			break;
		}
		case TYPE_REAL: {
			double *d = va_arg(arglist, double*);
			double value = parse_real(var->data, sizeof(uint16_t));
			*d = value;
			break;
		}
		case TYPE_STRING: {
			break;
		}
		case TYPE_INT: {
			int *i = va_arg(arglist, int*);
			memcpy(i, var->data, 16);
			break;
		}
	}
	va_end(arglist);
}