#ifndef JARGON_PARSER_H
#define JARGON_PARSER_H

#include "tree.h"

enum token_type_enum {
	TOKEN_COMMENT,
	TOKEN_STRING_LITERAL,
	TOKEN_CHARACTER_LITERAL,
	TOKEN_INTEGER_LITERAL,
	TOKEN_REAL_NUMBER_LITERAL,
	TOKEN_CURLY_BRACE_OPEN,
	TOKEN_CURLY_BRACE_CLOSE,
	TOKEN_PARENTHESIS_OPEN,
	TOKEN_PARENTHESIS_CLOSE,
	TOKEN_SQUARE_BRACE_OPEN,
	TOKEN_SQUARE_BRACE_CLOSE,
	TOKEN_WHITESPACE,
	TOKEN_OPERATOR,
	TOKEN_KEYWORD,
	TOKEN_IDENTIFIER,
	TOKEN_DATA_TYPE,
	TOKEN_END_INSTRUCTION
};

typedef struct {
	char text[200];
	uint16_t cursor;
	uint16_t length;
} code_line_t;

typedef struct {
	char word[100];
	byte token_type;
	bool parse_error;
} lexical_construct_t;

tree_node_binary_t *operator_binary_node(tree_node_binary_t *this_node, 
		tree_node_binary_t *left_node, tree_node_binary_t *right_node);

tree_node_binary_t *operator_unary_node(tree_node_binary_t *node);

tree_t *parse(code_line_t *expression);

lexical_construct_t lex(code_line_t *line);

lexical_construct_t parse_line(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_expression(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_term(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_literal(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_integer(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_real(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_char(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_string(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_bool(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_arguments(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_control(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_definition(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_parameter_list(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_id(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_assignment(code_line_t *line, 
		tree_node_binary_t *parent_node);

lexical_construct_t parse_list(code_line_t *line, 
		tree_node_binary_t *parent_node);

bool scan_reached_end(const code_line_t *line);

#endif