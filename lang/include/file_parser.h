#ifndef JARGON_FILE_PARSER_H
#define JARGON_FILE_PARSER_H

#include "def.h"

void parse_file(const char *path);

void clean_text(char *text, int max);

#endif