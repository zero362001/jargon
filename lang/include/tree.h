#ifndef JARGON_TREE_H
#define JARGON_TREE_H

#include "def.h"

typedef struct tree_node_binary {
	struct tree_node_binary *left_child;
	struct tree_node_binary *right_child;
	struct tree_node_binary *parent_node;
	void *data;
} tree_node_binary_t;

typedef struct {
	tree_node_binary_t *root_node;
	tree_node_binary_t *newest;
	uint32_t depth;
} tree_t;

tree_node_binary_t *add_node_left(tree_t *tree, tree_node_binary_t *parent_node, void *data);

tree_node_binary_t *add_node_right(tree_t *tree, tree_node_binary_t *parent_node, void *data);

tree_node_binary_t *add_root_node(tree_t *tree, void *data);

tree_node_binary_t *get_newest(const tree_t *tree);

tree_node_binary_t *get_root(const tree_t *tree);

void *get_data_root(const tree_t *tree);

void clear_subtree(tree_t *tree, tree_node_binary_t *subtree);

#endif
