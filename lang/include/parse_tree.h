#ifndef JARGON_PARSE_TREE_H
#define JARGON_PARSE_TREE_H

#include "tree.h"

enum expression_type_enum {
	EXPRESSION_ASSIGNMENT,
	EXPRESSION_OPERATION_ARITHMETIC_LOGIC_UNARY,
	EXPRESSION_OPERATION_ARITHMETIC_LOGIC_BINARY,
	EXPRESSION_DEFINITION,
	EXPRESSION_FUNCTION_DEFINITION,
	EXPRESSION_CONTROL_BRANCH,
	EXPRESSION_CONTROL_JUMP,
	EXPRESSION_DATA_LITERAL
};

typedef struct {
	byte expression_type;
	byte operation_code;
	byte data_type;
	void *data;
	void* (*evaluate) (tree_node_binary_t*);
} expression_t;

tree_node_binary_t *operation_node_binary(tree_node_binary_t *left,
		tree_node_binary_t *right);

tree_node_binary_t *operation_node_unary(tree_node_binary_t *node);

tree_node_binary_t *definition_node(tree_node_binary_t *signature,
		tree_node_binary_t *parameter_list);

tree_node_binary_t *control_node_branch(); // TODO: Implement!

tree_node_binary_t *control_node_jump(); // TODO: Implement!

void *operation_add(tree_node_binary_t *left, tree_node_binary_t *right);

void *operation_subtract(tree_node_binary_t *left, tree_node_binary_t *right);

void *operation_or(tree_node_binary_t *left, tree_node_binary_t *right);

void *operation_and(tree_node_binary_t *left, tree_node_binary_t *right);

void *operation_not(tree_node_binary_t *subtree);

void *operation_xor(tree_node_binary_t *left, tree_node_binary_t *right);

void *operation_allocate(tree_node_binary_t *identifier, 
		tree_node_binary_t *type);

void *operation_assign(tree_node_binary_t *identifier, 
		tree_node_binary_t *value);

void *evaluate_literal(tree_node_binary_t *node);

void *evaluate_addition(tree_node_binary_t *node);

void *evaluate_subtraction(tree_node_binary_t *node);

void *evaluate_multiplication(tree_node_binary_t *node);

void *evaluate_division(tree_node_binary_t *node);

void *evaluate_logical_and(tree_node_binary_t *node);

void *evaluate_logical_or(tree_node_binary_t *node);

void *evaluate_bitwise_and(tree_node_binary_t *node);

void *evaluate_bitwise_or(tree_node_binary_t *node);

void *evaluate_bitwise_xor(tree_node_binary_t *node);

void *evaluate_bitshift_right(tree_node_binary_t *node);

void *evaluate_bitshift_left(tree_node_binary_t *node);

void *evaluate_scope_resolution(tree_node_binary_t *node);

void *evaluate_function_call(tree_node_binary_t *node);

void *evaluate_logical_negate(tree_node_binary_t *node);

void *evaluate_arithmetic_negate(tree_node_binary_t *node);

void *evaluate_spec(tree_node_binary_t *node);

void *evaluate_assignment(tree_node_binary_t *node);

tree_node_binary_t *literal_node(void *data, byte type);

#endif