#ifndef JARGON_STACK_H
#define JARGON_STACK_H

#include "def.h"

struct list_node_struct;
struct list;

typedef struct stack {
	uint16_t stack_max_size;
	uint16_t stack_count;
	struct list *stack_items;
	struct list_node_struct *top;
} stack_t;

stack_t *create_stack(const uint16_t stack_size);

void *get_top(const stack_t *stack);

void *pop(stack_t *stack);

bool push(stack_t *stack, void *data);

void clear_stack(stack_t *stack);

#endif