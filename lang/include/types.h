// Temporarily implement types using C primitives,
// Migrate to use emulator later

#ifndef JARGON_TYPES_H
#define JARGON_TYPES_H

#include "def.h"

enum data_type_enum {
	TYPE_REAL,
	TYPE_INT,
	TYPE_STRING,
	TYPE_CHARACTER,
	TYPE_BOOLEAN
};

typedef struct {
	byte data_type;
	byte *data;
} variable_t;

void parse_variable(variable_t *var, ...);

void set_variable(variable_t *var, ...);

void set_variable_bytes(variable_t *var, const byte*, byte size);

void delete_variable(variable_t *var);

#endif