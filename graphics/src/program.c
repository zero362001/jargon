#include "program.h"
#include "window_handler.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>


static void render();
static void process_events();
static void update();
static void display();

int start_graphics_program(window_settings_t *settings) {
	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
	
	window_handler_t *window_handler = create_window(settings);
	SDL_Window *sdl_window = get_window(window_handler);
	SDL_Renderer *renderer = SDL_CreateRenderer(sdl_window, -1, 0);

	bool is_running = true;
	
	while (is_running) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				destroy_window(&window_handler);
				is_running = false;
				break;
			} else {
				process_events();
			}
		}
		update();
		render(renderer);
		display(renderer);
	}
	return RETURN_SUCCESS;
}

void process_events() {}

void update() {}

void render(SDL_Renderer *renderer) {
	SDL_RenderClear(renderer);
	SDL_Rect rect = { 0, 0, 100, 200 };
	SDL_RenderDrawRect(renderer, &rect);
}

void display(SDL_Renderer *renderer) {
	SDL_RenderPresent(renderer);
}