#include "window_settings.h"
#include "window_handler.h"
#include <stdlib.h>
#include <SDL2/SDL.h>

window_handler_t *create_window(window_settings_t *settings) {
	SDL_Window *sdl_window = SDL_CreateWindow(
			"SDL_Program", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
			settings->window_width, settings->window_height, settings->flags);
	window_handler_t *handler = malloc(sizeof(window_handler_t));
	handler->settings = settings;
	handler->window = sdl_window;
	return handler;
}

void destroy_window(window_handler_t **handler) {
	if (handler != nullptr && *handler != nullptr) {
		SDL_DestroyWindow((*handler)->window);
		free(*handler);
		*handler = nullptr;
	}
}

SDL_Window *get_window(window_handler_t *handler) {
	return handler->window;
}