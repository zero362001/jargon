#include "window_settings.h"
#include "def.h"

static window_settings_t default_settings;
static bool default_settings_initialised = false;

window_settings_t *get_default_settings() {
	if (!default_settings_initialised) {
		default_settings.flags = 0;
		default_settings.fps_limit = 0;
		default_settings.window_height = 800;
		default_settings.window_width = 800;
		default_settings_initialised = true;
	}
	return &default_settings;
}