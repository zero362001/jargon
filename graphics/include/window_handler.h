#ifndef JARGON_GRAPHICS_WINDOW_H
#define JARGON_GRAPHICS_WINDOW_H

struct window_settings;
struct SDL_Window;

typedef struct window {
	struct window_settings *settings;
	struct SDL_Window *window;
} window_handler_t;

window_handler_t* create_window(struct window_settings *settings);
void destroy_window(window_handler_t **window);
struct SDL_Window *get_window(window_handler_t *handler);

#endif