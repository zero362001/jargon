#ifndef JARGON_GRAPHICS_WINDOW_SETTINGS_H
#define JARGON_GRAPHICS_WINDOW_SETTINGS_H
#include "def.h"

typedef struct window_settings {
	char title[50];
	uint16_t window_height;
	uint16_t window_width;
	uint16_t fps_limit;
	uint16_t flags;
} window_settings_t;

window_settings_t *get_default_settings();

#endif