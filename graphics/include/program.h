#ifndef JARGON_GRAPHICS_PROGRAM_H
#define JARGON_GRAPHICS_PROGRAM_H

#include "window_settings.h"
#include "window_handler.h"

int start_graphics_program(window_settings_t *settings);

#endif