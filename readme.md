# Jargon

Jargon is an experimental, interpreted programming language written in C17, 
mainly as a learning experience as well as serving as a potential component for
one of my hacking simulation games.

Although it is one of my primary objectives to make it a general-purpose (in 
terms of computability) language, it is most certainly not intended to be a 
replacement for any other general-purpose programming language anytime soon.

Along with the interpreter, a compiled (bytecode running on a vm) version is 
also in the list of potential features.

This document aims to provide an overview of the language and its features.

>**NOTE:** Keeping in mind the fact that the language in its current state is only just 
an idea in my head, the information on this page describes the current proposal 
of the language and hence, most (if not all) of the content has either not yet 
been implemented or subject to change if already implemented. I shall use this 
page as a part of my dev-log and notebook. Most of what you see here atm. is 
for my own reference.

## Syntax

Jargon is syntactically influenced by a integer of languages such as Python, 
BASIC, Rust, Pascal, etc. to name a few. It is designed to be as simple and minimal 
as possible focusing primarily on human readability over everything else. All 
non-control statements, including jump commands, must be terminated with a 
semicolon.

### **Overview**
An example script showcasing the syntax of Jargon:
```
import "std/math";
import "std/console";

# Define a custom data structure describing a two-dimensional vector
# containing a cartesian coordinate

struct vector {

	x: real = 0;
	y: real = 0;

	# Define custom addition operation for our vector type, since Jargon doesn't
	# know how to add our vector type, yet

	operator addition() {
		x = left::x + right::x;
		y = left::y + right::y;
	}

	# Similar to the addition operation, define a subtraction operation

	operator subtraction() {
		x = left::x - right::x;
		y = left::y - right::y;
	}

}

# Define a function outside the custom vector type, unbound to the type definition,
# which takes two vectors (here, a and b) and returns the distance between them

function get_dist(a: vector, b: vector) {
	diff: vector = a - b;
	dist: real = math::sqrt(diff::x * diff::x + diff::y * diff::y);
	return dist;
}

# The main function is called when the program is first loaded into memory

function main() {
	a: vector = (2, 5);
	b: vector = (5, 6);
	console::print(get_dist(a, b));
}
```
<br>

### **Grammar**

statement = (expression | initialisation | keyword | keyword expression | declaration | control | definition), ";" ;
<br>
expression = factor, operator, expression | unary expression | factor | (, expression, ) ;
<br>
factor = identifier, (, arglist, ) | identifier | identifier, [, integer, ] | literal | {, arglist, } ;
<br>
literal = integer | real | ', character, ' | '"', string, '"' | true | false ;
<br>
string = { character } ;
<br>
integer = [ "-" ], { digit } ;
<br>
real = [ "-" ], (".", integer | integer, ".", integer) ;
<br>
arglist = expression, ",", arglist | expression ;
<br>
control = (control_keyword, (, expression, ) | for, (, identifier, in, expression, ) | else), "{", { statement }, "}" ;
<br>
definition = "function", identifier, (, params, ), ":", type ;
<br>
params = identifier, ":" type, ",", params | identifier, ":", type ;
<br>
keyword = break | pass | return, [ expression ] | import, '"', path, '"' ;
<br>
control_keyword = "if" | "match" | "while" | "elif" ;
<br>
identifier = (character | "_"), { character | digit | "_" } ;
<br>
operator = "+" | "-" | "::" | "&" | "&&" | "||" | "|" | "*" | "/" | "^" | ">>" | "<<" | "==" | "<=" | ">=" ;
<br>
unary = "-" | "!" ;
<br>
type = typename | typename, "[", integer, "]" ;
<br>
typename = "int" | "char" | "string" | "real" | "bool" | identifier ;
<br>
intialisation = identifier, ":", type, "=", expression | identfier, "=", expression ;