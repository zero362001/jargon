#ifndef VM_MEMORY_H
#define VM_MEMORY_H

#include "def.h"
    
#define WORD_SIZE_16

#if defined(WORD_SIZE_16)
    typedef uint16_t word;
    #define WORD_SIZE 16U
#elif defined(WORD_SIZE_32)
    typedef uint32_t word;
    #define WORD_SIZE 32U
#elif defined(WORD_SIZE_64)
    typedef uint64_t word;
    #define WORD_SIZE 64U
#else
    typedef uint8_t word;
    #define WORD_SIZE 8
#endif

#define MEM_SIZE = ((word)1 << WORD_SIZE - 1)
#define PAGE_SIZE 1 << sizeof(byte) * 8
#define PAGE_COUNT = MEM_SIZE / PAGE_SIZE;

typedef struct block {
    struct block *next;
    struct block *prev;
    word *address;
    word size;
} block_t;

typedef struct page {
    struct page *next;
    struct page *prev;
    byte data[PAGE_SIZE];
} page_t;

typedef struct {
    byte data;
    word address_next;
} data_address_tuple_t;

typedef struct {
    page_t *page_head;
    page_t *page_tail;
    word page_count;
} memory_t;

/// @param memory The memory instance where to allocate data
/// @param size the size of memory to allocate (in bytes)
/// @brief Returns the address of a newly allocated block of @b 'size'.
/// Returns nullptr if no available memory
word allocate(memory_t *memory, const word size);

/// @param memory The memory instance from where to read data
/// @param location the location of the byte in memory
/// @brief Returns a tuple containing the byte at that memory, and the 
/// address of the byte right next to it, if any in case of reading data
/// longer than a single byte (16 bit integers, structures)
data_address_tuple_t read(memory_t *memory, const word location);

/// @param memory The memory instance where to write data
/// @param location the location where to write the data
/// @param data the bytes to write at the given location
/// @param size the number of bytes to write
/// @brief Writes a number of bytes at the given memory location
bool write(memory_t *memory, const word location, byte data[], const word size);

#endif
